Categories:Theming
License:MIT
Web Site:http://alexanderfedora.blogspot.com
Source Code:https://github.com/ghisguth/sunlight/tree/master/sun
Issue Tracker:https://github.com/ghisguth/sunlight/issues

Auto Name:Your very own Sun!
Summary:Live wallpaper
Description:
Live animated sun wallpaper that is configurable: You can change size, color,
animation speed, corona size, turbulence and many more options.
.

Repo Type:git
Repo:https://github.com/ghisguth/sunlight.git

Build:1.0,1
    commit=9457a7ebd586b6aa00159fd1c7ac11852a88c38a
    subdir=sun

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
