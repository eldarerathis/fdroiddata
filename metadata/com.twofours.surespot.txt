AntiFeatures:NonFreeDep
Categories:Phone & SMS
License:GPLv3
Web Site:https://www.surespot.me/
Source Code:https://github.com/surespot/android
Issue Tracker:https://github.com/surespot/android/issues

Name:Surespot
Auto Name:surespot
Summary:Send encrypted text messages
Description:
Use surespot and everything you send can only be read by the person you sent it
to. Period. Everything sent using surespot is end-to-end encrypted with
symmetric-key encryption (256 bit AES-GCM) using keys created with 521 bit ECDH
shared secret derivation, but you wont notice because security in surespot is
built-in, not a layer over something else. Be confident sending private
information and pictures, you have control over your messages, when you delete a
sent message it will be removed from the receiver's phone and images are not
shareable unless you make them so. Multiple identities allow you to be who you
want with just who you want, and if anyone gets annoying you can block them.
Surespot does not require or store your phone number or email address and we
don't mine your data, no advertisements here!
.

Repo Type:git
Repo:https://github.com/surespot/android.git

Build:61-fdroid,61
    disable=builds, but includes jars
    commit=4b9219e86c62208ebe1ca0cf48afa0cf15c34251
    subdir=surespot
    gradle=yes
    forceversion=yes
    prebuild=rm 'src/main/res/raw/ffmpegpie' && \
        rm 'src/main/res/raw/ffmpeg' && \
        touch 'src/main/res/raw/ffmpegpie' && \
        touch 'src/main/res/raw/ffmpeg' && \
        sed -i -e 's#com.google.android.gms:play-services-drive:7.5.0#com.android.support:support-v4:22.2.0#' build.gradle && \
        mkdir -p ../surespotcommon/src/main/res/raw && \
        sed -i -e 's#Build.VERSION_CODES.M#23#' src/main/java/com/twofours/surespot/identity/IdentityController.java && \
        printf 'ssl_strict=true\nbaseUrl=https://server.surespot.me:443\n' > ../surespotcommon/src/main/res/raw/configuration.properties && \
        sed -i -e 's#import java.util.Properties;#import java.util.Properties;\nimport com.twofours.surespot.common.R;\n#g' ../surespotcommon/src/main/java/com/twofours/surespot/common/SurespotConfiguration.java && \
        pushd src/main/java/com/twofours/surespot/backup/ && \
        sed -i -e '/import com.google.android.gms/d' -e 's#GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE#"dummy"#' -e '/AccountPicker.newChooseAccountIntent/d' ImportIdentityActivity.java ExportIdentityActivity.java && \
        cat ImportIdentityActivity.java | grep -v '\snull);' > ImportIdentityActivity.java_ && \
        mv ImportIdentityActivity.java_ ImportIdentityActivity.java && \
        sed -i -e 's#ActivityNotFoundException e#Exception e#' ExportIdentityActivity.java ImportIdentityActivity.java && \
        sed -i -e 's#startActivityForResult(accountPickerIntent, SurespotConstants.IntentRequestCodes.CHOOSE_GOOGLE_ACCOUNT);#System.out.println("dummy");#' ExportIdentityActivity.java ImportIdentityActivity.java && \
        popd && \
        sed -i -e 's#android {#android {\nlintOptions {\nabortOnError false\n}\n#' build.gradle

Maintainer Notes:
* Voice function removed

./surespot/libs/google-api-services-drive-v2-rev103-1.17.0-rc.jar
./surespot/libs/jsr305-1.3.9.jar
./surespot/libs/google-oauth-client-1.17.0-rc.jar
./surespot/libs/google-api-client-android-1.17.0-rc.jar
./surespot/libs/google-api-client-1.17.0-rc.jar
./surespot/libs/google-http-client-1.17.0-rc.jar
./surespot/libs/google-http-client-android-1.17.0-rc.jar
./surespot/libs/gcm.jar

.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:63
Current Version Code:63
